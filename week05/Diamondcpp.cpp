#include<iostream>
using namespace std;
class person
{
    public:
    person()
    {
        cout<<" grand parent "<<endl;
    }
};
class father:virtual public person
{
    public:
    father()
    {
        cout<<" father  "<<endl;
    }
};
class mother: virtual public person
{
    public:
    mother()
    {
        cout<<" mother"<<endl;
    }
};
class child:public father,public mother
{
    public:
    child()
    {
        cout<<"child"<<endl;
    }
};
int main()
{
    child obj;
    return 0;
}