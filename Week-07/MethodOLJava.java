public class MethodOLJava {
    void display(float a) {
        System.out.println("I am float");
    }

    void display(char c) {
        System.out.println("I am character");
    }

    public static void main(String args[]) {
        MethodOLJava obj = new MethodOLJava();
        obj.display(8);
        obj.display('s');
    }
}