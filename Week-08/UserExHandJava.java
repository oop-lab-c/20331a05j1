
import java.util.*;  
class UserExHandJava{  
    public static void main(String[] args){  
        try{  
            throw new MyException(10);  
        }  
        catch(MyException ex){  
            System.out.println(ex) ;  
        }
        finally
        {
            System.out.println("\nFinished");
        }
    }  
}  
class MyException extends Exception{  
    int a;  
    MyException(int b) {  
        a=b;  
    }  
    public String toString(){  
        return ("Exception value =  "+a) ;  
    }  
}  
